annotate CatalogService.Books with @(
	UI: {
	    SelectionFields: [ ID ],
	        LineItem: [
	        	{ Value: ID, Label: 'Book ID' },
		        { Value: title, Label: 'Title' },
		        { Value: descr, Label: 'Descrizione' },
		        { Value: author.ID, Label: 'Author ID' },
		        { Value: author.name, Label: 'Author' },
		        { Value: stock, Label: 'Stock Quantity' },
		        { Value: price, Label: 'Price' },
		        { Value: currency.code, Label: 'Currency' }
	    	],
	        HeaderInfo: {
			    TypeName: 'Books',
			    TypeNamePlural: 'Books',
			    Description: { Value: title }
		    }
	}
);