const cds = require('@sap/cds')
module.exports = async function (){

  const db = await cds.connect.to('db')
  const { Books } = db.entities

  this.on ('submitOrder', async req => {
    const {book,amount} = req.data
    const affectedRows = await UPDATE (Books, book)
      .with ({ stock: {'-=': amount }})
      .where ({ stock: {'>=': amount }})
    affectedRows > 0 || req.error (409,`${amount} exceeds stock for book #${book}`)
  })

  // Saldi
  this.after ('READ','Books', each => {
    if (each.stock > 111) {
      each.title += ` -- 11% discount!`
    }
  })
  
  this.on ('sumNumber', (req, err) => {
  	const {a, b} = req.data
  	return {
  		sum : a + b,
  		concat: "a: " + a.toString() + ", b: " + b.toString()
  	}
  })
}