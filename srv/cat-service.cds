using { my.bookshop as my } from '../db/data-model';

service CatalogService {
	  //@requires: 'system-user'
	  entity Authors @readonly as projection on my.Authors;
	  entity Orders @insertonly as projection on my.Orders;
	  
	  @readonly entity Books as SELECT from my.Books {*} excluding { createdBy, modifiedBy };
	  
	  action submitOrder (book : Books.ID, amount: Integer);
	  
	  type ReturnType {
	  	sum: Integer;
	  	concat: String;
	  }
	  function sumNumber(a: Integer, b: Integer) returns ReturnType;
}